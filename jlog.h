﻿#ifndef JLOG_H
#define JLOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QLayout>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrlQuery>
#include <QMouseEvent>

typedef struct SelectPic
{
    QLabel *Label;
    int X;
    int Y;
}SelectPic;

#define TITLEHIGH   25          // 验证码提示标题高度

enum ResultCode{
    LOGIN = 0,      // 登录成功
    RIGHT = 4,      // 验证码成功
    ERROR = 5       // 验证码失败
};

namespace Ui {
class JLog;
}

class JLog : public QDialog
{
    Q_OBJECT

public:
    explicit JLog(QWidget *parent = 0);
    ~JLog();

signals:
    void SendLogInfo(QString User, QString Passwd);

private slots:
    void OnpBtnLog();

    void ReceiveNetworkInfo(QNetworkReply *Info);

    void mousePressEvent(QMouseEvent * e);

    void ObpBtnRefresh();

private:
    Ui::JLog *ui;
    QLabel * LbLogo;                    // Logo
    QLabel * LbServer;
    QLabel * LbUser;
    QLabel * LbPasswd;
    QLabel * LbTipInfo;
    QLabel * LbVerCode;                 // 验证码
    QComboBox * cBoxServer;
    QComboBox * cBoxUser;
    QLineEdit * lEdtPasswd;
    QPushButton * pBtnLog;
    QGridLayout * gLayInfo;
    QPushButton * pBtnRefresh;          // 验证码刷新按钮
    QNetworkAccessManager * Manage;
    QString Coords;
    QVector<SelectPic> AuthCodeList;

    void Init();
    void InitCtrls();
};

#endif // JLOG_H
