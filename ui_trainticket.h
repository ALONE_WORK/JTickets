/********************************************************************************
** Form generated from reading UI file 'trainticket.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRAINTICKET_H
#define UI_TRAINTICKET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TrainTicket
{
public:
    QTabWidget *tabWidget;
    QWidget *tab_ticket;
    QWidget *tab_orders;

    void setupUi(QWidget *TrainTicket)
    {
        if (TrainTicket->objectName().isEmpty())
            TrainTicket->setObjectName(QStringLiteral("TrainTicket"));
        TrainTicket->resize(802, 515);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font.setPointSize(12);
        TrainTicket->setFont(font);
        tabWidget = new QTabWidget(TrainTicket);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(20, 40, 761, 451));
        tabWidget->setTabPosition(QTabWidget::West);
        tabWidget->setTabShape(QTabWidget::Triangular);
        tab_ticket = new QWidget();
        tab_ticket->setObjectName(QStringLiteral("tab_ticket"));
        tabWidget->addTab(tab_ticket, QString());
        tab_orders = new QWidget();
        tab_orders->setObjectName(QStringLiteral("tab_orders"));
        tabWidget->addTab(tab_orders, QString());

        retranslateUi(TrainTicket);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TrainTicket);
    } // setupUi

    void retranslateUi(QWidget *TrainTicket)
    {
        TrainTicket->setWindowTitle(QApplication::translate("TrainTicket", "TrainTicket", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_ticket), QApplication::translate("TrainTicket", "\346\237\245\350\257\242", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_orders), QApplication::translate("TrainTicket", "\350\256\242\345\215\225\347\256\241\347\220\206", 0));
    } // retranslateUi

};

namespace Ui {
    class TrainTicket: public Ui_TrainTicket {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRAINTICKET_H
